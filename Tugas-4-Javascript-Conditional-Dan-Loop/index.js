// JAWABAN SOAL 1
console.log('');
console.log('JAWABAN SOAL 1')

var nilai = 90;

if (nilai >= 85) {
    console.log("Mendapat Nilai Index A")
} else if (nilai >= 75 && nilai < 85) {
    console.log("Mendapat Nilai Index B")
} else if (nilai >= 65 && nilai < 75) {
    console.log("Mendapat Nilai Index C")
} else if (nilai >= 55 && nilai < 65) {
    console.log("Mendapat Nilai Index D")
} else {
    console.log("Mendapat Nilai Index E")
}

//JAWABAN SOAL 2
console.log('');
console.log('JAWABAN SOAL 2')

var tanggal = 02;
var bulan = 12;
var tahun = 1995;

switch (bulan) {
    case 1: { console.log(tanggal + ' Januari ' + tahun); break; }
    case 2: { console.log(tanggal + ' Februari ' + tahun); break; }
    case 3: { console.log(tanggal + ' Maret ' + tahun); break; }
    case 4: { console.log(tanggal + ' April ' + tahun); break; }
    case 5: { console.log(tanggal + ' Mei ' + tahun); break; }
    case 6: { console.log(tanggal + ' Juni ' + tahun); break; }
    case 7: { console.log(tanggal + ' Juli ' + tahun); break; }
    case 8: { console.log(tanggal + ' Agustus ' + tahun); break; }
    case 9: { console.log(tanggal + ' September ' + tahun); break; }
    case 10: { console.log(tanggal + ' Oktober ' + tahun); break; }
    case 11: { console.log(tanggal + ' November ' + tahun); break; }
    case 12: { console.log(tanggal + ' Desember ' + tahun); break; }
    default: { console.log('Maaf, bulan hanya ada 12 saja, koreksi kembali'); }
}

//JAWABAN SOAL 3
console.log('');
console.log('JAWABAN SOAL 3')
var output = '';
var n = 3;
for (var x = 0; x < n; x++) {
    for (var y = 0; y <= x; y++) {
        output += '* ';
    }
    output += '\n';
}
console.log('output untuk n = 3')
console.log(output);

var output2 = '';
var a = 7;
console.log('');
var output2 = '';
for (var b = 0; b < a; b++) {
    for (var c = 0; c <= b; c++) {
        output2 += '* ';
    }
    output2 += '\n';
}
console.log('output untuk n = 7')
console.log(output2);

//JAWABAN SOAL 3
console.log('');
console.log('JAWABAN SOAL 4')
var n = new Array('Programming', 'Javascript', 'Vue JS');
var m = 3;
for (i = 0; i <= m; i++) {
    console.log(i + " Saya Suka " + n[i]);
}


