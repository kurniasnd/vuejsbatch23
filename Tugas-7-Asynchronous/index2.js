// di index.js
var readBooks = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

readBooks(9000, books[0])
    .then(sisaWaktu => readBooks(sisaWaktu, books[1]))
    .then(sisaWaktu => readBooks(sisaWaktu, books[2]))
    .catch(err => console.log("waktu kurang dari " + err))
