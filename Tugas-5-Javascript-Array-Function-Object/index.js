// Soal 1
var daftarBuah = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarBuah.sort();
for (i = 0; i < daftarBuah.length; i++) {
    console.log(daftarBuah[i]);
}


//Soal 2
var data = {
    name: "Sandi",
    age: 25,
    address: "Malang Jawa Timur",
    hobby: "Hiking"
}
function introduce(data) {
    return (
        "Nama Saya " + data.name +
        ", umur saya" + data.age +
        ", alamat saya di " + data.address +
        ", dan saya punya hobby yaitu " + data.hobby
    )
}

var perkenalan = introduce(data);
console.log(perkenalan);

//Soal 3
var vocal = ["a", "i", "u", "e", "o"];
var res = 0;

function hitung_huruf_vokal(nama) {
    for (var i = 0; i < nama.length; i++) {
        if (vocal.includes(nama[i])) res++;
    }
    return res;
}

var hitung_1 = hitung_huruf_vokal("kurnia");
var hitung_2 = hitung_huruf_vokal("sandi");
console.log(hitung_1, hitung_2);

//Soal 4
function hitung(angka) {
    return angka * 2 - 2;
}

console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));

