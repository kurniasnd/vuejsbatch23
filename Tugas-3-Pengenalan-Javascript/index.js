//Soal 1
var pertama = "saya sangat senang hari ini ";
var kedua = "belajar javascript itu keren ";

var a = pertama.substring(0, 5);
var b = pertama.substring(12, 19);
var c = kedua.substring(0, 8);
var d = kedua.substring(8, 18);
var upperD = d.toUpperCase();

console.log("//Soal 1");
console.log(a.concat(b, c, upperD));

//Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var strInt_1 = parseInt(kataPertama);
var strInt_2 = parseInt(kataKedua);
var strInt_3 = parseInt(kataKetiga);
var strInt_4 = parseInt(kataKeempat);

var result = (strInt_1 + strInt_4) + (strInt_2 * strInt_3);

console.log("//Soal 2");
console.log(result);

//Soal 3
var kalimat = 'wah javascript itu keren sekali';
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("//Soal 3");
console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);
