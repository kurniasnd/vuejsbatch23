//Soal 1
let luasPersegi = function (p, l) {
    return p * l;
};
console.log('luas persegi =', luasPersegi(10, 5));

let kelilingPersegi = function (p, l) {
    return 2 * (p + l);
};

console.log('keliling persegi =', kelilingPersegi(10, 5));

//Soal 2
newFunction = (firstName, lastName) => {
    firstName
    lastName
    return {
        fullName() {
            return console.log(firstName + " " + lastName)
        }
    }
}
newFunction("William", "Imoh").fullName();


//Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const { firstName, lastName, address, hobby } = newObject;

//Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];

console.log(combined);

//Soal 5
const planet = 'earth'
const view = 'glass'
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`
console.log(before);
